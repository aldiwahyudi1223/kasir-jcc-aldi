<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Profile;
use App\Models\Barang;
use App\Models\Pembelian;
use App\Models\Pembelianbarang;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $user = User::count();
        $profile = Profile::count();
        $mbarang = Barang::count();
        $tpembelian = Pembelian::count();
        $tpembelianbarang = Pembelianbarang::count();

        return view('dashboard', compact('user', 'profile', 'Barang', 'Pembelian', 'Pembelianbarang'));
    }
}