   <!-- Brand Logo -->
   <a href="index3.html" class="brand-link">
    <img src="dist/img/AdminLTELogo.png" alt="jCC" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Projec JCC</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{ url('dashboard') }}" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-header">MASTER DATA</li>
          <li class="nav-item">
            <a href="{{ url('master-barang') }}" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Data Barang
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('user') }}" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Data Pengguna
              </p>
            </a>
          </li>
        <li class="nav-header">TRANSAKSI</li>
          <li class="nav-item">
            <a href="{{ url('transaksi-pembelian-barang') }}" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Pesanan
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('transaksi-pembelian') }}" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Pembayaran
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-shopping-cart"></i>
                <span>Transaksi</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Custom Transaksi:</h6>
                    <a class="collapse-item" href="{{ url('transaksi-pembelian-barang') }}">Daftar Transaksi <br> Pembelian
                        Barang</a>
                    <a class="collapse-item" href="{{ url('transaksi-pembelian') }}">Total Akhir <br> Transaksi
                        Pembelian</a>
    
                </div>
            </div>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
{{--  --}}